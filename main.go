package main

import (
    "fmt"
    "flag"
    "net/http"
    "html/template"
    "github.com/zenazn/goji"
    "github.com/zenazn/goji/web"
)

func index(c web.C, w http.ResponseWriter, r *http.Request) {
    Title := "OLA K ASE!!!"
    Body := "mmm lol jaja"
    t, _ := template.ParseFiles("tpl/index.html")
    t.Execute(w, map[string]interface{}{
        "Title": Title,
        "Body" : Body,
    })
}

func hello(c web.C, w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hello, %s!", c.URLParams["name"])
}

func main() {
    goji.Get("/", index)
    goji.Get("/hello/:name", hello)
    flag.Set("bind", ":8080")
    goji.Serve()
}
